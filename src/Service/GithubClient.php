<?php

namespace App\Service;

use Github\Client;

class GithubClient
{
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getUserApi()
    {
        return $this->client->api('user');
    }

    public function getRepoApi()
    {
        return $this->client->api('repo');
    }

    public function findUser(string $username): array
    {
        return $this->getUserApi()->find($username);
    }

    public function getUserRepositories(string $username): array
    {
        return $this->getUserApi()->repositories($username, 'all', 'updated', 'desc');
    }

    public function getRepositoriesLanguage(string $username): array
    {
        $data = [];
        foreach ($this->getUserRepositories($username) as $repos) {
            $repoLanguages = $this->getRepoApi()->languages($username, $repos['name']);
            foreach ($repoLanguages as $languages => $value) {
                $data[$languages] = isset($data[$languages]) ? $data[$languages] + 1 : 1;
                continue;
            }
        }

        return $data;
    }
}