<?php


namespace App\Service;

class UserResponseBuilder
{
    private $user;
    private $data = [];

    public function __construct(array $user)
    {
        $this->user = $user;
    }

    public function appendLocation(): UserResponseBuilder
    {
        $this->data['location'] = $this->user['location'];

        return $this;
    }

    public function appendRepositoriesCount(): UserResponseBuilder
    {
        $this->data['repositoriesCount'] = $this->user['repos'];

        return $this;
    }

    public function appendFollowersCount(): UserResponseBuilder
    {
        $this->data['followersCount'] = $this->user['followers'];

        return $this;
    }

    public function appendName(): UserResponseBuilder
    {
        $this->data['name'] = $this->user['name'];

        return $this;
    }

    public function appendJoinDate(): UserResponseBuilder
    {
        $this->data['joinDate'] = (new \DateTime($this->user['created_at']))->format('Y');

        return $this;
    }

    public function getResponseData(): array
    {
        return $this->data;
    }
}