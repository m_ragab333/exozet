<?php

namespace App\Service;

class ResponseDecorator
{
    private $githubClient;

    public function __construct(GithubClient $githubClient)
    {
        $this->githubClient = $githubClient;
    }

    public function getUserDetails(string $username): array
    {
        $userDetails = $this->githubClient->findUser($username)['users'][0];

        $userResponseBuilder = new UserResponseBuilder($userDetails);
        return $userResponseBuilder->appendJoinDate()
            ->appendName()
            ->appendLocation()
            ->appendFollowersCount()
            ->appendRepositoriesCount()
            ->getResponseData();
    }

    public function getReposDetails(string $username): array
    {
        $repositoriesDetails = $this->githubClient->getUserRepositories($username);

        $repositories = [];
        foreach ($repositoriesDetails as $repositoriesDetail) {
            $repositoriesResponseBuilder = new RepositoryResponseBuilder($repositoriesDetail);
            $repositories[] = $repositoriesResponseBuilder->appendName()
                ->appendDescription()
                ->appendUrl()
                ->appendForksCount()
                ->appendStarsCount()
                ->appendPushedAt()
                ->appendCreatedAt()
                ->getResponseData();
        }

        $starsCount = array_column($repositories, 'starsCount');
        array_multisort($starsCount, SORT_DESC, $repositories);

        return $repositories;
    }

    public function getLanguageDetails(string $username): array
    {
        $languages = $this->githubClient->getRepositoriesLanguage($username);

        return $this->getPercentage($languages);

    }

    private function getPercentage(array $array): array
    {
        $total = array_sum($array);

        $percentageValues = [];

        foreach ($array as $key => $item) {
            $percentageValues[$key] = ceil(($item / $total) * 100);
        }

        arsort($percentageValues);

        return $percentageValues;
    }
}