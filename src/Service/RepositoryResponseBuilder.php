<?php


namespace App\Service;

class RepositoryResponseBuilder
{
    private $repository;
    private $data = [];

    public function __construct(array $repository)
    {
        $this->repository = $repository;
    }

    public function appendName(): RepositoryResponseBuilder
    {
        $this->data['name'] = $this->repository['name'];

        return $this;
    }
    public function appendDescription(): RepositoryResponseBuilder
    {
        $this->data['description'] = $this->repository['description'];

        return $this;
    }

    public function appendUrl(): RepositoryResponseBuilder
    {
        $this->data['url'] = $this->repository['html_url'];

        return $this;
    }

    public function appendCreatedAt(): RepositoryResponseBuilder
    {
        $this->data['createdAt'] = (new \DateTime($this->repository['created_at']))->format('Y');

        return $this;
    }

    public function appendPushedAt(): RepositoryResponseBuilder
    {
        $this->data['pushedAt'] = (new \DateTime($this->repository['pushed_at']))->format('Y');

        return $this;
    }

    public function appendForksCount(): RepositoryResponseBuilder
    {
        $this->data['forksCount'] = $this->repository['forks_count'];

        return $this;
    }

    public function appendStarsCount(): RepositoryResponseBuilder
    {
        $this->data['starsCount'] = $this->repository['stargazers_count'];

        return $this;
    }

    public function getResponseData(): array
    {
        return $this->data;
    }
}