<?php

namespace App\Controller;

use App\Form\Type\GithubType;
use App\Service\ResponseDecorator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GithubController extends AbstractController
{
    private $responseDecorator;

    //TODO: Write some tests
    public function __construct(ResponseDecorator $responseDecorator)
    {
        $this->responseDecorator = $responseDecorator;
    }

    /**
     * @Route("/github/home")
     */
    public function index(Request $request)
    {
        $form = $this->createForm(GithubType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $username = $data['username'];

            //TODO: cache results maybe in Redis, to speed up the response time
            $user = $this->responseDecorator->getUserDetails($username);
            $repos = $this->responseDecorator->getReposDetails($username);
            $languages = $this->responseDecorator->getLanguageDetails($username);

            //TODO: to be implemented
            $contributions = [];

            //TODO: to be implemented
            $organizations = [];

            return $this->render('github/index.twig', [
                'user' => $user,
                'languages' => $languages,
                'repositories' => $repos,
                'contributions' => $contributions,
                'organizations' => $organizations
            ]);
        }

        return $this->render('github/form.twig', [
            'form' => $form->createView(),
        ]);
    }
}